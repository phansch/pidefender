PiDefender
==========

My January entry for One Game A Month 2013

## Story ##

>Our empire is under attack!  
  
>Your scout ship is directing our people's only defense:  
>A giant defensive cannon that is the only hope.
  
>Try not to get hit by enemy fire while killing as many as possible.

Story will be updated and refined during the month.

## Credits ##

 * http://opengameart.org/content/20-crosshairs-for-re
 * Lorc's RPG icon set http://opengameart.org/content/700-rpg-icons